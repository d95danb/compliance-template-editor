module Routing exposing (..)

import Navigation exposing (Location)

type RoutePart
    = CategoryRoute String
    | QuestionRoute String
    | OptionsRoute String
    | GroupQuestion String
    | FollowUpQuestion String

type alias Route = List RoutePart

parseLocation : Location -> List RoutePart
parseLocation location = 
    let
        parse xss =
            case xss of 
                "categories"         :: categoryName :: xs    -> CategoryRoute    categoryName :: parse xs
                "questions"          :: questionId   :: xs    -> QuestionRoute    questionId   :: parse xs
                "subquestions"       :: questionId   :: xs    -> GroupQuestion    questionId   :: parse xs
                "options"            :: optionsId    :: xs    -> OptionsRoute     optionsId    :: parse xs
                "followup-questions" :: questionId   :: xs    -> FollowUpQuestion questionId   :: parse xs
                _ -> []
    in
        parse <| String.split "/" (String.dropLeft 1 location.hash) 
