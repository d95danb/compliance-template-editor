module Optics exposing (..)

import Monocle.Optional exposing (Optional)
import Monocle.Common exposing ((=>))

type alias Focus a b = { optional : Optional a b, path : List String }

(==>) : Focus a b -> Focus b c -> Focus a c
(==>) f1 f2 = { optional = f1.optional => f2.optional, path = f1.path ++ f2.path }

elementOfElementList : (a -> Bool) -> Optional (List a) a
elementOfElementList predicate = 
    let 
        tryFind xs = List.filter predicate xs |> List.head
        addOrReplace x_ xss = 
            case xss of
                [] -> [x_]
                x::xs -> if predicate x then x_ :: xs else x :: addOrReplace x_ xs
    in
        Optional tryFind addOrReplace

