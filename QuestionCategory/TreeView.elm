module QuestionCategory.TreeView exposing (questionCategoriesView)

import ViewComponent exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import ViewComponent exposing (..)
import Question.Model exposing (..)
import Question.TreeView exposing (..)
import Question.Optics exposing (questionOfQuestionList)
import Bootstrap.ListGroup as ListGroup
import Optics exposing (..)
import ViewHelpers exposing (..)
import Material.Icons.Action as Icons
import QuestionCategory.Model exposing (..)
import QuestionCategory.Optics exposing (..)

questionCategoriesView : ViewComponent (List QuestionCategory) am msg
questionCategoriesView = eval <| \model modelTraits focus questionCategories ->
    let
        makeQuestionCategoryView : QuestionCategory -> ListGroup.Item msg
        makeQuestionCategoryView questionCategory =
            let
                questionCategoryName = toString questionCategory.categoryType
                focus_ = focus ==> questionCategoryOfQuestionCategoryList questionCategory.categoryType
                setActiveFlag isActive = modelTraits.makeMsg <| (focus_ ==> activeOfQuestionCategory).optional.set isActive
                addNewQuestionToCategory = 
                    modelTraits.makeIdMsg <| \id -> (focus_ ==> questionsOfQuestionCategory ==> questionOfQuestionList id).optional.set (makeQuestion id)
                address = href ("#" ++ String.join "/" focus_.path)                
            in
                ListGroup.li [ ListGroup.attrs [style [("border","0")]] ] 
                    [ p [ onMouseOver (setActiveFlag True), onMouseOut (setActiveFlag False) ]
                        [ treeIcon Icons.class questionCategory.active
                        , Html.a [ address ] [ text questionCategoryName ]
                        , treeViewButtons [("Add Question", addNewQuestionToCategory)] questionCategory
                        ]
                    , questionsView model modelTraits (focus_ ==> questionsOfQuestionCategory)            
                    ]                
    in
        div [] 
            [ ListGroup.ul 
                <| List.map makeQuestionCategoryView questionCategories
            ]
