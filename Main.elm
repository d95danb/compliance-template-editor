import Html exposing (Html, div, text, h1)
import Html.Attributes exposing (style, class)
import Bootstrap.CDN as CDN
import Bootstrap.Grid as Grid
import Navigation exposing (Location)
import Monocle.Optional exposing (Optional)
import Monocle.Lens exposing (Lens)
import Routing exposing (..)
import Uuid exposing (Uuid, uuidGenerator)
import Random.Pcg exposing (Seed, step, initialSeed)
import Question.Model exposing (..)
import Question.Optics exposing (..)
import Question.QuestionDetailsView exposing (..)
import Question.OptionDetailsView exposing(..)
import QuestionCategory.Model exposing (..)
import QuestionCategory.Optics exposing (..)
import QuestionCategory.DetailsView exposing (..)
import Optics exposing (..)
import JsonModel.DetailsView exposing (..)
import QuestionTemplate.Model exposing (..)
import QuestionTemplate.Views exposing (..)
import QuestionTemplate.Optics exposing (..)
import ViewHelpers exposing (..)

-- Program entry point

main : Program Never Model Msg    
main = Navigation.program OnLocationChange { init = init, view = view, update = update, subscriptions = subscriptions }

-- Types

type alias Model = { route : Route, uuidSeed : Seed, questionTemplate : QuestionTemplate }

type Msg = UpdateModel (Model -> Model)
         | OnLocationChange Location

type SelectedEntity a = Question (Focus a Question)
                      | Option (Focus a QuestionOption)
                      | Category (Focus a QuestionCategory )
                      | None

-- Id generation

type alias Seedable a = { a | uuidSeed : Seed }

withNewId : (String -> Seedable a -> Seedable a) -> Seedable a -> Seedable a
withNewId f model =
            let
                (newUuid, newSeed) =
                    step uuidGenerator model.uuidSeed
            in
                f (Uuid.toString newUuid) { model | uuidSeed = newSeed } 

-- Optics

questionTemplateOfModel : Focus Model QuestionTemplate
questionTemplateOfModel = {
        optional = Lens (\m -> m.questionTemplate) (\c m -> { m | questionTemplate = c }) |> Monocle.Optional.fromLens,
        path = [ ]
    }

-- Helpers

routeToSelectedEntity : Route -> SelectedEntity Model
routeToSelectedEntity = 
    let
        categoryRouteToSelectedEntity : Route -> SelectedEntity Model
        categoryRouteToSelectedEntity route =
            case route of 
                (CategoryRoute categoryTypeName)::xs -> 
                    let
                        focus_ categoryType = questionTemplateOfModel ==> categoriesOfQuestionTemplate ==> questionCategoryOfQuestionCategoryList (categoryType)
                        makeResult categoryType = 
                            case xs of 
                                [] -> Category <| focus_ categoryType
                                _  -> questionRouteToSelectedEntity xs (focus_ categoryType)
                    in                
                        case String.toUpper categoryTypeName of
                            "PEP" -> makeResult PEP
                            "AML" -> makeResult AML
                            _ -> Debug.log ("No route found for category " ++ categoryTypeName) None
                _ -> None

        questionRouteToSelectedEntity : Route -> Focus a QuestionCategory -> SelectedEntity a
        questionRouteToSelectedEntity route focus =
            case route of 
                (QuestionRoute questionId)::xs -> 
                    let
                        focus_ = focus ==> questionsOfQuestionCategory ==> questionOfQuestionList questionId
                    in                
                        case xs of 
                            [] -> Question <| focus_
                            _  -> subQuestionRouteToSelectedEntity xs focus_
                _ -> None

        subQuestionRouteToSelectedEntity : Route -> Focus a Question -> SelectedEntity a
        subQuestionRouteToSelectedEntity route focus =
            case route of 
                (OptionsRoute optionId)::xs -> 
                    let
                        focus_ = focus ==> questionTypeOfQuestion ==> selectQuestionOfQuestion ==> optionsOfSelectQuestion ==> optionOfOptionsList optionId
                    in                
                        case xs of 
                            [] -> Option <| focus_
                            _  -> optionRouteToSelectedEntity xs focus_
                (GroupQuestion questionId)::xs ->
                    let
                        focus_ = focus ==> questionTypeOfQuestion ==> questionGroupOfQuestion ==> questionsOfGroupQuestion ==> questionOfQuestionList questionId
                    in                
                        case xs of 
                            [] -> Question <| focus_
                            _  -> subQuestionRouteToSelectedEntity xs focus_
                _ -> None

        optionRouteToSelectedEntity : Route -> Focus a QuestionOption -> SelectedEntity a
        optionRouteToSelectedEntity route focus =
            case route of 
                (FollowUpQuestion questionId)::xs -> 
                    let
                        focus_ = focus ==> followUpQuestionsOfOption ==> questionOfQuestionList questionId
                    in                
                        case xs of 
                            [] -> Question <| focus_
                            _  -> subQuestionRouteToSelectedEntity xs focus_
                
                _ -> None
        
    in        
        categoryRouteToSelectedEntity 

-- Init

init : Location -> ( Model, Cmd Msg )
init location = 
    let
        initialModel currentRoute = 
            { route = currentRoute
            , uuidSeed = initialSeed 0
            , questionTemplate =  
                { name = ""                    
                , categories= [ makeQuestionCategory PEP, makeQuestionCategory AML ]
                }
            }
    in
        (initialModel <| parseLocation location, Cmd.none)


-- Subscriptions

subscriptions : Model -> Sub Msg
subscriptions model = Sub.none

-- Update

update : Msg -> Model -> (Model, Cmd Msg)
update msg model = 
    let 
        updateModel msg model =
            case msg of 
                UpdateModel f -> f model
                OnLocationChange newLocation -> { model | route = parseLocation newLocation }
    in
        (updateModel msg model, Cmd.none)

-- Views

view model = 
    let
        rootView : Html Msg
        rootView = 
            let
                modelTraits = { makeMsg = UpdateModel, makeIdMsg = UpdateModel << withNewId }
                questionTemplateView_ = questionTemplateView model modelTraits questionTemplateOfModel
                questionDetailsView_ = 
                    case routeToSelectedEntity model.route of 
                        Category focus -> questionCategoryDetailsView model modelTraits focus
                        Question focus -> questionDetailsView model modelTraits focus
                        Option focus -> optionDetailsView model modelTraits focus
                        _ -> text ""
                jsonModelView_ = jsonModelView model modelTraits questionTemplateOfModel
            in
                Grid.container [ style [("background-color", "#333"), ("max-width", "100%")]]  -- #e10075,  #6b1faf
                    [ CDN.stylesheet -- creates an inline style node with the Bootstrap CSS
                    , div [ class "row" ]
                        [ div [ class "col-7" ]
                            [ questionTemplateView_ ]
                        , div [ class "col-5"] 
                            [ questionDetailsView_  ]
                        ]
                    , div [ class "row"]
                        [ div [ class "col" ] 
                            [ jsonModelView_ ]
                        ]                
                    ]
    in
        rootView
